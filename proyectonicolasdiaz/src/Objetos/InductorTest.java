package Objetos;

public class InductorTest {

	public static void main(String[] args) {
		//crear mi primier
		Inductor inductor1 = new Inductor();
		Inductor inductor2 = new Inductor("L2", 0.03f);
		Inductor inductor3 = new Inductor();
		
		
		inductor3.setNombre("L3");
		inductor3.setValor(0.1f);
	
		
		System.out.println("Inductor 1 " + inductor1);
		System.out.println("Inductor 2 " + inductor2);
		System.out.println("Inductor 3 " + inductor3);

		System.out.println("comparacion");
		
		System.out.println("inductor 1 y 2 " + inductor1.equals(inductor2));
		inductor1 = new Inductor("L2", 0.03f);
		
		System.out.println("inductor 1 y 2 " + inductor1.equals(inductor2));

		// para L2
		inductor2.setFrecuencia(1000);
		System.out.println("el inductor 2 tiene una reactancia:" + inductor2.calcularImpedancia());
		System.out.println("inductor2=" + inductor2);
		
		
	}

}

