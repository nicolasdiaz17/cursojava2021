package Objetos;

	import static org.junit.Assert.*;

	import java.util.ArrayList;
	import java.util.List;

	import org.junit.After;
	import org.junit.Before;
	import org.junit.Test;

	import Objetos.Capacitor;
	import Objetos.Componente; 
	import Objetos.Inductor;
	import Objetos.Resistencia;

	public class InductorTesteo {
		//defino el lote de pruebas
		Inductor inductorVacio;
	    Inductor inductorConDatos;
	    
	    Capacitor capacitorVacio;
	    Capacitor capacitorConDatos;
	    
	    Resistencia resistenciaVacia;
	    Resistencia resistenciaConDatos;
	    
	    List<Componente> componentes;
	    List<Componente> componentes2;
	    
		@Before
		public void setUp() throws Exception {
			//se ejecuta antes de cada testeo
			//creo los objetos
			inductorVacio 		= new Inductor()				 ;
			inductorConDatos 	= new Inductor("L1",0.003f, 3000);
		
			capacitorVacio 		= new Capacitor();
			capacitorConDatos   = new Capacitor("C1", 0.00001f,1000);
			
			resistenciaVacia	= new Resistencia();
			resistenciaConDatos = new Resistencia("R1", 100);
			
			componentes = new ArrayList<>();		
			componentes.add(new Inductor("indu1", 0.1f, 100));
			componentes.add(new Inductor("indu2", 0.2f, 200));
			componentes.add(new Inductor("indu3", 0.3f, 300));
			componentes.add(new Inductor("indu4", 0.4f, 400));
			componentes.add(new Inductor("indu5", 0.5f, 500));
			componentes.add(new Inductor("indu6", 0.6f, 600));
			componentes.add(new Inductor("indu7", 0.7f, 700));
			componentes.add(new Inductor("indu8", 0.8f, 800));
			componentes.add(new Inductor("indu9", 0.9f, 900));
			componentes.add(new Inductor())					;
			
			componentes.add(new Capacitor("cap1",0.001f,1000));
			componentes.add(new Capacitor("cap2",0.002f,2000));
			componentes.add(new Capacitor("cap3",0.003f,3000));
			componentes.add(new Capacitor("cap4",0.004f,4000));
			componentes.add(new Capacitor("cap5",0.005f,5000));
			componentes.add(new Capacitor("cap6",0.006f,6000));
			componentes.add(new Capacitor("cap7",0.007f,7000));
			componentes.add(new Capacitor());
			
			componentes.add(new Resistencia("Res1", 100));
			componentes.add(new Resistencia("Res2", 200));
			componentes.add(new Resistencia("Res3", 300));
			componentes.add(new Resistencia("Res4", 400));
			componentes.add(new Resistencia("Res5", 500));
			componentes.add(new Resistencia("Res6", 600));
			componentes.add(new Resistencia());
			
			componentes2 = new ArrayList<>();
			componentes2.add(inductorConDatos);
			
			
			
		}

		@After
		public void tearDown() throws Exception {
			//despues de cada testeo
			inductorVacio		=null;
			inductorConDatos	=null;
			
			capacitorVacio  	=null;
			capacitorConDatos 	=null;
			
			resistenciaVacia 	=null;
			resistenciaConDatos =null;
			
			componentes			=null;
		}
		
		@Test
		public void testEquals_Capacitor_Contains_TRUE(){
			Capacitor otro = new Capacitor();
			assertTrue(componentes.contains(otro));
		}
		
		@Test
		public void testEquals_Capacitor_Contains_FALSE(){
			Capacitor otro = new Capacitor();
			otro.setFrecuencia(7000);
			assertFalse(componentes.contains(otro));
		}
		
		//voy a probar solamente el nombre ustedes deberian porbar el resto de los atribuos
		@Test
		public void testCapacitorNombre(){
			assertEquals("cap_def", capacitorVacio.getNombre());
		}

		@Test
		public void testInductorNombre() {
			assertEquals("L por defecto", inductorVacio.getNombre());
		}
		
		@Test
		public void inductConDatosNombre() {
			assertEquals("L1", inductorConDatos.getNombre());
		}
		
		@Test
		public void testInductoUnidad() {
			assertEquals("Hy", inductorVacio.getUnidad());
			
		}
		@Test
		public void testinductConDatosUnidad() {
		
			assertEquals("Hy", inductorConDatos.getUnidad());
		}
		@Test
		public void testInductorInductancia(){
			assertEquals(0.01f, inductorVacio.getValor(), 0.001);
		}
		@Test
		public void testinductConDatosInductancia(){
			assertEquals(0.003f, inductorConDatos.getValor(), 0.0003);
		}
		
		
		@Test
		public void testInductorFrecuencia() {
			assertEquals(1000, inductorVacio.getFrecuencia());
			
		}
		
		@Test
		public void testInductConDatosFrecuencia() {
			assertEquals(3000, inductorConDatos.getFrecuencia());
			
		}
		
		@Test
		public void testCalcularImpedancia() {
			assertEquals(62.83f, inductorVacio.calcularImpedancia(), 0.01);
			
		}
		@Test
		public void testCalcularInductConDatosImpedancia() {
			assertEquals(56.55f,inductorConDatos.calcularImpedancia(),0.003); 
			
		}
		//TODO alumnos, prueben el inductConDatosImpedancia
		@Test
		public void testEqalsTRUE(){
			Inductor otro = new Inductor();
			assertTrue(inductorVacio.equals(otro));
		}
		@Test
		public void testInductConDatosEquasTrue(){
			Inductor otro = inductorConDatos;
			assertTrue(inductorConDatos.equals(otro));
		}
		
		@Test
		public void testEqalsFALSE(){
			Inductor otro = new Inductor();
			otro.setNombre("otro");
			assertFalse(inductorVacio.equals(otro));
		}
		@Test
		public void testInductConDatosEquasFalse(){
			Inductor otro = new Inductor();
			otro.setNombre("L2");
			assertFalse(inductorConDatos.equals(otro));
		}
		
		@Test
		public void testEqualsEnListaTRUE(){
			assertTrue(componentes.contains(inductorVacio));
		}
		
		@Test
		public void testInductConDatosListaEquasTrue(){
			assertTrue(componentes2.contains(inductorConDatos));
		}
		
		@Test
		public void testInductorConDatosEqualsEnListaFALSE (){
			assertFalse(componentes.contains(inductorConDatos));
		}
		
		@Test
		public void testEqualsEnListaFALSE(){
			assertFalse(componentes2.contains(inductorVacio));
		}


}