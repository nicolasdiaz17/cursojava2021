package Objetos;


	public class Capacitor extends Componente {
		//atributo
	    private int frecuencia;
	    
	    //constructores
		public Capacitor() {
			super("cap_def", 0.000001f, "F");
			frecuencia = 1000;
		}

		public Capacitor(String pNombre, float pValor, int pFrecuencia) {
			super(pNombre, pValor, "F");
			frecuencia = pFrecuencia;
			
		}

		//metodo de negocio
		@Override
		public float calcularImpedancia() {		
			return 1/(2*(float)Math.PI*getValor());
		}

		//accessors getter y setter
		public int getFrecuencia() {					return frecuencia;				}
		public void setFrecuencia(int frecuencia) {		this.frecuencia = frecuencia;	}
		
		public boolean equals(Object obj){
			boolean bln = false;
			if (obj!=null && obj instanceof Capacitor){
				//downcas
				Capacitor cap = (Capacitor) obj;
				bln = super.equals(obj)  				&&
					 cap.getFrecuencia() == frecuencia	;	
			}
			return bln;
		}
		public int hashCode(){
			return super.hashCode() + (int)frecuencia;
		}
		public String toString(){
			StringBuilder sb = new StringBuilder(super.toString());
			sb.append(",frecuencia=");
			sb.append(frecuencia);
			return sb.toString();
		}
		

	}

