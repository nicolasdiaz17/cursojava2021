package Objetos;

/**
 * Esta clase tiene la finalidad de guardar todos loas datos correspondientes a un inductor
 * 
 * @author 
 *
 */
public class Inductor extends Componente{
	private int 	frecuencia;
	
	
	//construcor y es el primer metodo que se ejecuta 
	//cada vez que se crea un objeto
	public Inductor(){
		//es colocarle aca un valor por defecto
		super("L por defecto", 0.01f, "Hy");
		frecuencia = 1000;
		
	}
	public Inductor(String pNombre, float pInduc ){
		super(pNombre, pInduc, "Hy");
	}
	
	
	
	public Inductor(String nombre, float inductancia, int frecuencia) {
		super(nombre, inductancia, "Hy");
		this.frecuencia = frecuencia;
	}
	
	//motodos de negocio
	
	public float calcularImpedancia(){
		return 2*(float)Math.PI*frecuencia*getValor();
	}
	
	
	
	public int getFrecuencia() {
		return frecuencia;
	}
	public void setFrecuencia(int frecuencia) {
		this.frecuencia = frecuencia;
	}
	public boolean equals(Object obj){
		boolean bln=false;
		if (obj !=null && obj instanceof Inductor){
			Inductor indu = (Inductor) obj;
			bln = super.equals(indu) 						&&
				  indu.getFrecuencia() == this.frecuencia;
		}
		return bln;
	}
	
	public int hashCode(){
		return super.hashCode() + this.frecuencia;
	}
	public String toString(){
		StringBuilder sb = new StringBuilder(super.toString());
		sb.append(", frecuencia=");
		sb.append(frecuencia);
		return sb.toString();
	}
	
}



