package Objetos;

public abstract class Componente {
	private String nombre;
	private float valor;
	private String unidad;
	
	public Componente() {
		super();		
	}
	public Componente(String nombre, float valor, String unidad) {
		super();
		this.nombre = nombre;
		this.valor = valor;
		this.unidad = unidad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public String getUnidad() {
		return unidad;
	}
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	//metodo de negocio como obligacion hacia los hijos
	public abstract float calcularImpedancia();
	
	public boolean equals(Object obj){
		boolean bln = false;
		if(obj!=null && obj instanceof Componente){
			Componente comp = (Componente) obj;
			bln = comp.getNombre().equals(nombre) 	&&
				  comp.getValor() == valor 			&&
				  comp.getUnidad().equals(unidad);
		}
		return bln;
	}
	public int hashCode(){
		return nombre.hashCode() + (int)valor + unidad.hashCode();
	}
	public String toString(){
		StringBuilder sb = new StringBuilder("nombre=");
		sb.append(nombre);
		sb.append(", valor=");
		sb.append(valor);
		sb.append(",unidad=");
		sb.append(unidad);
		return sb.toString();
	}
	
	

}

