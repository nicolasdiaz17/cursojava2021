package modulo4;

import java.util.Scanner;

public class ejercicio16
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el n�mero del que quiere visualizar su tabla.");
		int n = scan.nextInt();
		
		System.out.println("La tabla del " + n + " es:");
		
		for(int i=1; i<11; i++ )
		{
			int r = n * i;
			System.out.println(n + "x" + i + "=" + r);
		}
		scan=null;
	}
}
