package modulo4;

import java.util.Scanner;

public class ejercicio14 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese el puesto obtenido");
		int puesto = scan.nextInt();
		
		switch (puesto)
		{
		case 1:
			System.out.println("¡Felictaciones! Usted ha ganado la medalla de oro.");
			break;
		case 2:
			System.out.println("¡Felictaciones! Usted ha ganado la medalla de plata.");
			break;
		case 3:
			System.out.println("¡Felictaciones! Usted ha ganado la medalla de bronce.");
			break;
		default:
			System.out.println("¡Enhorabuena! Usted no ha ganado ninguna medalla, siga participando.");
			break;
		}
		scan=null;
	}
}
