package modulo4;

import java.util.Scanner;

public class ejercicio5 {

	public static void main(String[] args) {
		
		try (Scanner scan = new Scanner(System.in)) {
			System.out.println("Ingrese el puesto obtenido");
			int puesto =scan.nextInt();
			
			if(puesto == 1)
				System.out.println("¡Felictaciones! Usted ha ganado la medalla de oro.");
			else if(puesto == 2)
				System.out.println("¡Felictaciones! Usted ha ganado la medalla de plata.");
			else if(puesto == 3)
				System.out.println("¡Felictaciones! Usted ha ganado la medalla de bronce.");
			else
				System.out.println("¡Enhorabuena! Usted no ha ganado ninguna medalla, siga participando.");
			
		}

	}

}
