package modulo4;

import java.util.Scanner;

public class ejercicio4 {

	public static void main(String[] args) {
		
		try (Scanner scan = new Scanner(System.in)) {
			System.out.println("Ingrese una catergoria entre A, B o C");
			String categoria = scan.nextLine();
			
			if (categoria.equals("A") || categoria.equals("a"))
				System.out.println("Hijo");
			if (categoria.equals("B") || categoria.equals("b"))
				System.out.println("Padres");
			if (categoria.equals("C") || categoria.equals("c"))
				System.out.println("Abuelos");
		}
	}

}
