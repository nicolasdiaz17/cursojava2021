package modulo4;

import java.util.Scanner;

public class ejercicio17B 
{
	public static void main(String[] args) 
	{
			System.out.println("Ingrese el n�mero del que quiere visualizar su tabla.");
			Scanner scan = new Scanner(System.in);
			int n = scan.nextInt();
			int sumano=0;
			int suma=0;
			
			System.out.println("La tabla del " + n + " es:");
			
			for(int i=1; i<11; i++ )
			{
				int r = n * i;
				System.out.println(n + "x" + i + "=" + r);
				int par = r %2;
				suma = suma + r;
				sumano = sumano + (r * par);
			}
			
			int sumpar = suma - sumano;
			
			System.out.println("La suma de todos los n�meros pares es " + sumpar);
			
			scan=null;
	}
}
