package modulo4;

import java.util.Scanner;

public class ejercicio18 {

	public static void main(String[] args) 
	{
		System.out.println("Ingrese el n�mero del que quiere visualizar su tabla.");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		
		for(int i=1; i<n+1; i++ )
		{
			System.out.println("Tabla del " + i);
			for(int u=1; u<11; u++ )
			{
				int r = i * u;
				System.out.println(i + "x" + u + "=" + r);
			}
			
		}
	}

}
