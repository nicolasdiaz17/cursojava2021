package modulo4;

import java.util.Scanner;

public class ejercicio13 {

	public static void main(String[] args) 
		{
			Scanner scan = new Scanner(System.in);
			System.out.println("Ingrese el n de mes (1 a 12)");
			int mes = scan.nextInt();
			
			switch (mes)
			{
			case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				System.out.println("El mes ingresado (" + mes + ") tiene 31 dias");
				break;
			case 2: 
				System.out.println("El mes ingresado (" + mes + ") tiene 28 dias");
				break;
			case 4: case 6: case 9: case 11:
				System.out.println("El mes ingresado (" + mes + ") tiene 30 dias");
				break;
			
			default:
				System.out.println("�El n�mero no pertenece a ning�n mes o es inv�lido!");
				break;
			}
			scan=null;
		}
	}
