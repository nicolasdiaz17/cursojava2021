package modulo4;

public class ejercicio20
{
	public static void main(String[] args)
	{
		int n = 0;
		int mayor = 0;
		int menor = 1001;
		
		while (n<10)
		{
			int na = (int) Math.floor(Math.random()*1000);
			System.out.println("El numero aleatoriamente generado es " + na);	
			if (na < menor)
				menor = na;
			if (na > mayor)
				mayor = na;
			n = n + 1;
		}
		System.out.println("El menor numero es " + menor);
		
		System.out.println("El mayor numero es " + mayor);	
	}

}
