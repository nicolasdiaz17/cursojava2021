package modulo4;

import java.util.Scanner;

public class ejercicio3 {

	public static void main(String[] args) {

		try (Scanner scan = new Scanner(System.in)) {
			System.out.println("Ingrese un mes");
			String mes = scan.nextLine();
			
			if (mes.equals("Enero") || mes.equals("Marzo") || mes.equals("Mayo")|| mes.equals("Julio") || mes.equals("Agosto") || mes.equals("Octubre") || mes.equals("Diciembre") || mes.equals("enero") || mes.equals("marzo")|| mes.equals("mayo") || mes.equals("julio") || mes.equals("agosto") || mes.equals("octubre") || mes.equals("diciembre"))
				System.out.println("El mes ingresado tiene 31 dias");
			else if (mes.equals("Febrero") || mes.equals("febrero"))
				System.out.println("El mes ingresado tiene 28 dias");
			else if (mes.equals("Abril") || mes.equals("Junio") || mes.equals("Septiembre") || mes.equals("Noviembre") || mes.equals("abril") || mes.equals("junio") || mes.equals("septiembre") || mes.equals("noviembre"))
				System.out.println("El mes ingresado tiene 30 dias");  
			else
				System.out.println("El mes ingresado es desconocido");
		}
	}

}