package modulo4;

import java.util.Scanner;

public class ejercicio17A
{

	public static void main(String[] args) 
	{
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el n�mero del que quiere visualizar su tabla.");
		int n = scan.nextInt();
		int sumpar = 0;
		
		System.out.println("La tabla del " + n + " es:");
		
		for(int i=1; i<11; i++ )
		{
			int r = n * i;
			System.out.println(n + "x" + i + "=" + r); 
			if (r%2==0) sumpar = sumpar + r ;
		}
		
		System.out.println("La suma de todos los n�meros pares es " + sumpar);
		
			
	} 
		

}