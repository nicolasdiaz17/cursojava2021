package modulo4;

import java.util.Scanner;

public class ejercicio12 {

	public static void main(String[] args) {
	
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un n�mero entero �nicamente");
		int n = scan.nextInt();
		
		if (n >= 1 && n <= 12)
			System.out.println("El n�mero pertenece a la primer docena.");
		else if (n >= 12 && n <= 24)
			System.out.println("El n�mero pertenece a la segunda docena.");
		else if (n >= 24 && n <= 36)
			System.out.println("El n�mero pertenece a la tercer docena.");
		else if (n < 1 || n > 36 )
			System.out.println("�El n�mero ingresado no pertenece a ninguna docena o es inv�lido!");
		
		scan=null;
	}
}
