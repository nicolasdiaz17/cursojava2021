package modulo4;

import java.util.Scanner;

public class ejercicio15
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la clase de auto (A, B o C)");
		char clase = scan.next().charAt(0);
		
		switch (clase)
		{
		case 'A': case 'a':
			System.out.println("La clase A de autos posee: 4 ruedas y un motor");
			break;
		case 'B': case 'b':
			System.out.println("La clase B de autos posee: 4 ruedas, un motor, cierre centralizado y aire");
			break;
		case 'C': case 'c':
			System.out.println("La clase C de autos posee: 4 ruedas, un motor, cierre centralizado, aire y airbag.");
			break;
		default:
			System.out.println("�La clase ingresada es inv�lida!");
			break;
		}
		scan=null;
	}
}
