package modulo4;

import java.util.Scanner;

public class ejercicio6 {

	public static void main(String[] args) {
		
		try (Scanner scan = new Scanner(System.in)) {
			System.out.println("Ingrese el N� del curso actual");
			int curso =scan.nextInt();
			
			if(curso == 0)
				System.out.println("Cursando actualmente: Jard�n de infantes.");
			else if(curso >= 1 && curso <= 6)
				System.out.println("Cursando actualmente: Escuela Primaria.");
			else if(curso >= 7 && curso <= 12)
				System.out.println("Cursando actualmente: Escuela Secundaria.");
			else if (curso <= 13)
				System.out.println("El curso ingresado no es v�lido");
				;
		}
	}

}
