package modulo4;

import java.util.Scanner;

public class ejercicio8 {

	public static void main(String[] args) {

		try (Scanner scan = new Scanner(System.in)) {
			System.out.println("Ingrese jugada del jugador A ( 0 = PIEDRA ;  1 = PAPEL ;  2 = TIJERA )");
			float JugadaA =scan.nextFloat();
			
			System.out.println("Ingrese jugada del jugador B ( 0 = PIEDRA ;  1 = PAPEL ;  2 = TIJERA )");
			float JugadaB =scan.nextFloat();
			
			if (JugadaA <3 && JugadaB <3 && JugadaA == JugadaB) 
				System.out.println("EMPATE");
				
			else 
			{
			   		if (JugadaA == 0 && JugadaB == 1)
			   			System.out.println("El jugador B es el ganador");
			   else if (JugadaA == 0 && JugadaB == 2)
				   		System.out.println("El jugador A es el ganador");
			   else if (JugadaA == 1 && JugadaB == 0)
				   		System.out.println("El jugador A es el ganador");
			   else if (JugadaA == 1 && JugadaB == 2)
				   		System.out.println("El jugador B es el ganador");
			   else if (JugadaA == 2 && JugadaB == 0)
			   	   		System.out.println("El jugador B es el ganador");
			   else if (JugadaA == 2 && JugadaB == 1)
				   		System.out.println("El jugador A es el ganador");
			   else if (JugadaB == JugadaA && JugadaB >=3)
						System.out.println("Valores ingresados por los jugadores �NO V�LIDOS!");
			   else if (JugadaA >= 3)
						System.out.println("Valor ingresado por el jugador A �NO V�LIDO!");
			   else if (JugadaB >= 3)
			    		System.out.println("Valor ingresado por el jugador B �NO V�LIDO!");
			   		
			}
		}
	}
}