package modulo4;

import java.util.Scanner;

public class ejercicio11 {

	public static void main(String[] args) {
		{
			Scanner scan = new Scanner(System.in);
			System.out.println("Ingrese una letra");
			char letra = scan.next().charAt(0);
			
			if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u' || letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O' || letra == 'U')
				System.out.println("La letra ingresada es una vocal");
			else if (letra == 'b' || letra == 'c' || letra == 'd' || letra == 'f' || letra == 'g' || letra == 'h' || letra == 'j' || letra == 'k' || letra == 'l' || letra == 'm' || letra == 'n' || letra == '�' || letra == 'p' || letra == 'q' || letra == 'r' || letra == 's' || letra == 't' || letra == 'v' || letra == 'w' || letra == 'x' || letra == 'y' || letra == 'z' || letra == 'B' || letra == 'C' || letra == 'D' || letra == 'F' || letra == 'G' || letra == 'H' || letra == 'J' || letra == 'K' || letra == 'L' || letra == 'M' || letra == 'N' || letra == '�' || letra == 'P' || letra == 'Q' || letra == 'R' || letra == 'S' || letra == 'T' || letra == 'V' || letra == 'W' || letra == 'X' || letra == 'Y' || letra == 'Z')
				System.out.println("La letra ingresada NO es una vocal");
			else
			    System.out.println("El caracter ingresado no es una letra");
			scan=null;
		}
	}
}